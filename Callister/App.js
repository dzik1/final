import React from "react";
import { StyleSheet, View, Text } from "react-native";
import Screen from "./Callister";
import firebase from "firebase/app";
import "firebase/auth";
//import "firebase/database";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCtj_A3QrQbkfZimqXjBVrhueoKGU_KdqQ",
  authDomain: "callister-b0ce5.firebaseapp.com",
  projectId: "callister-b0ce5",
  storageBucket: "callister-b0ce5.appspot.com",
  messagingSenderId: "264868548441",
  appId: "1:264868548441:web:9ce308fe762bcbb111c648",
};

firebase.initializeApp(firebaseConfig);

export default function App() {
  return <Screen />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
