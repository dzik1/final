import React, { useEffect } from "react";
import { StyleSheet, ImageBackground, Image, Text, View } from "react-native";

const SplashSceen = ({ navigation }) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace("LoginScreen");
    }, 3000);
  }, [{ navigation }]);

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../Asset/bg1.png")}
        style={styles.image}
      >
        <View>
          <Image style={styles.logo} source={require("../../Asset/Logo.png")} />
        </View>
        <Text style={styles.text}>callister.id</Text>
      </ImageBackground>
    </View>
  );
};

export default SplashSceen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
  },
  logo: {
    width: 120,
    height: 120,
    marginBottom: 20,
    marginLeft: 25,
  },
  text: {
    color: "#676161",
    fontSize: 30,
    fontWeight: "bold",
    paddingLeft: 20,
    marginBottom: 30,
  },
});
