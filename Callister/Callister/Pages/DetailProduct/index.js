import React from "react";
import { StyleSheet, Text, TouchableOpacity, View, Image } from "react-native";

export default function DetailScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.navigate("MainApp")}>
          <Text style={{ fontSize: 14, fontWeight: "bold" }}>Go Back</Text>
        </TouchableOpacity>
      </View>
      <View styles={styles.image}>
        <Image
          style={styles.imageDetail}
          source={require("../../Asset/taichan.png")}
        />
      </View>
      <View style={styles.conten}>
        <Text style={{ fontSize: 25, fontWeight: "bold" }}>Taichan Satay</Text>
        <View style={styles.price}>
          <TouchableOpacity>
            <Image
              style={styles.plus}
              source={require("../../Asset/mins.png")}
            />
          </TouchableOpacity>
          <Text
            style={{ paddingHorizontal: 20, fontSize: 30, fontWeight: "bold" }}
          >
            1
          </Text>
          <TouchableOpacity>
            <Image
              style={styles.mins}
              source={require("../../Asset/plus.png")}
            />
          </TouchableOpacity>
          <Text
            style={{ alignSelf: "flex-start", fontSize: 22, marginLeft: "30%" }}
          >
            Rp. 50.000
          </Text>
        </View>
        <View style={styles.line}></View>
        <View style={styles.pack}>
          <Text style={styles.packTitle}>One Pack Contains:</Text>
          <View
            style={{ height: 2, width: "50%", backgroundColor: "#35A882" }}
          ></View>
          <Text style={styles.packDesc}>
            A pack of taichan satay, a plate of rice, and a glass of sweet iced
            tea.
          </Text>
          <View style={styles.line}></View>
          <Text style={{ fontSize: 15, color: "grey", fontStyle: "italic" }}>
            order now and feel the pleasure!
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#B5E7DB",
    alignItems: "center",
  },
  header: {
    margin: 40,
    backgroundColor: "white",
    width: 80,
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    alignSelf: "flex-start",
  },
  image: {
    padding: 50,
  },
  imageDetail: {
    width: 145,
    height: 147,
  },
  conten: {
    marginTop: "12%",
    padding: 25,
    width: "100%",
    height: "60%",
    borderTopStartRadius: 30,
    borderTopEndRadius: 30,
    backgroundColor: "#E7EFF4",
  },
  price: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 13,
  },
  plus: {
    width: 39,
    height: 47,
  },
  mins: {
    width: 39,
    height: 47,
  },
  line: {
    marginVertical: 10,
    backgroundColor: "#CCD0D2",
    height: 1,
  },
  packTitle: {
    fontSize: 20,
    paddingBottom: 10,
  },
  packDesc: {
    marginVertical: 15,
    fontSize: 18,
  },
});
