import React, { useState } from "react";
import {
  StyleSheet,
  ImageBackground,
  Image,
  Text,
  View,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  onPress,
} from "react-native";

import firebase from "firebase/app";

export default function LoginScreen({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);

  const submit = () => {
    const Data = {
      email,
      password,
    };
    console.log(Data);

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((userCredential) => {
        // Signed in
        var user = userCredential.user;
        console.log(user);
        navigation.navigate("MainApp", {
          username: email,
        });
        // ...
      })
      .catch((error) => {
        var errorCode = error.code;
        var errorMessage = error.message;
      });
  };

  return (
    <ImageBackground
      source={require("../../Asset/bg2.png")}
      style={styles.image}
    >
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <View style={styles.loginContainer}>
            <Text style={styles.titlelogin}>Login</Text>
            <View style={styles.form}>
              <View style={styles.fromContainer}>
                <TextInput
                  style={styles.fromInput}
                  placeholder="Email"
                  value={email}
                  onChangeText={(value) => setEmail(value)}
                />
                <TextInput
                  style={styles.fromInput}
                  placeholder="Password"
                  value={password}
                  onChangeText={(value) => setPassword(value)}
                />
              </View>
            </View>
            <View style={styles.bottomContainer}>
              <View>
                <TouchableOpacity>
                  <Text
                    style={{ color: "white", fontSize: 15, fontWeight: "bold" }}
                  >
                    Forgot Password?
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity style={styles.buttonLogin} onPress={submit}>
                  <Text
                    style={{ color: "white", fontSize: 20, fontWeight: "bold" }}
                  >
                    Login
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </SafeAreaView>
        <SafeAreaView style={styles.footer}>
          <TouchableOpacity>
            <Image
              style={styles.iconGoogle}
              source={require("../../Asset/iconGoogle.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image
              style={{ marginLeft: 40 }}
              source={require("../../Asset/iconFb.png")}
            />
          </TouchableOpacity>
          <View style={styles.titleFooter}>
            <Text style={{ color: "white", fontSize: 16 }}>New Here?</Text>
            <TouchableOpacity
              onPress={() => navigation.navigate("RegisterScreen")}
            >
              <Text
                style={{ color: "white", fontSize: 16, fontWeight: "bold" }}
              >
                Register
              </Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </ScrollView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    marginTop: 150,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  },
  loginContainer: {
    margin: 20,
  },
  titlelogin: {
    color: "white",
    fontSize: 36,
    fontWeight: "bold",
    marginTop: 50,
    marginBottom: 30,
  },
  fromContainer: {
    marginBottom: 20,
  },
  fromInput: {
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 8,
    padding: 10,
    margin: 10,
  },
  bottomContainer: {
    flexDirection: "row",
    paddingTop: 15,
  },
  buttonLogin: {
    padding: 10,
    paddingHorizontal: 40,
    marginLeft: 70,
    borderRadius: 8,
    backgroundColor: "#C1EFE0",
    alignItems: "center",
  },
  footer: {
    flex: 1,
    flexDirection: "row",
    paddingBottom: 20,
    margin: 20,
    marginTop: 50,
  },
  titleFooter: {
    alignItems: "flex-start",
    marginLeft: 60,
    paddingTop: 10,
    color: "white",
  },
});
