import React from "react";
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";

import { Recomended } from "../../Data/index";
import { Product } from "../../Data/index";

export default function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity>
          <View style={styles.list}>
            <Image
              style={{ width: 25, height: 25 }}
              source={require("../../Asset/list.png")}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.basket}>
            <Image
              style={{ height: 30, width: 30 }}
              source={require("../../Asset/basket.png")}
            />
            <Text>My Basket</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={{ paddingTop: 10 }}>
        <Text style={styles.headerTitle}>
          Hello Dzikri, What food combo do you want today?
        </Text>
      </View>
      <View style={styles.search}>
        <Image
          style={{
            width: 22,
            height: 22,
            marginHorizontal: 15,
            alignSelf: "center",
          }}
          source={require("../../Asset/search.png")}
        />
        <TextInput
          style={styles.serachInput}
          placeholder="Search for fruit salad combos"
        />
        <Image
          style={{ width: 36, height: 30, alignSelf: "center", marginLeft: 55 }}
          source={require("../../Asset/filter.png")}
        />
      </View>
      <View style={styles.combo}>
        <Text style={styles.comboTitle}>Recommended Combo</Text>
        <FlatList
          data={Recomended}
          horizontal
          renderItem={({ item }) => (
            <TouchableOpacity
              style={styles.productCard}
              onPress={() => navigation.navigate("DetailScreen")}
            >
              <Image style={styles.loveProduct} source={item.love} />
              <Image style={styles.productImage} source={item.image} />
              <Text style={styles.productTitle}>{item.title}</Text>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.productPrice}>Rp. {item.harga},-</Text>
                <TouchableOpacity>
                  <Image style={styles.plusProduct} source={item.plus} />
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
      <View style={styles.product}>
        <View style={styles.listKategori}>
          <TouchableOpacity>
            <Text style={{ fontSize: 17 }}>Hottest</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={{ fontSize: 17, paddingLeft: 28 }}>Popular</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={{ fontSize: 17, paddingLeft: 28 }}>New Combo</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={{ fontSize: 17, paddingLeft: 28 }}>Top</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          data={Product}
          horizontal
          renderItem={({ item }) => (
            <TouchableOpacity
              style={styles.productCardList}
              onPress={() => navigation.navigate("DetailScreen")}
            >
              <Image style={styles.loveProductList} source={item.love} />
              <Image style={styles.productImageList} source={item.image} />
              <Text style={styles.productTitleList}>{item.title}</Text>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.productPriceList}>Rp. {item.harga},-</Text>
                <TouchableOpacity>
                  <Image style={styles.plusProductList} source={item.plus} />
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    backgroundColor: "#E7EFF4",
  },
  header: {
    flexDirection: "row",
    paddingTop: 10,
  },
  basket: {
    alignItems: "center",
    paddingLeft: 260,
  },
  headerTitle: {
    fontSize: 25,
  },
  search: {
    flexDirection: "row",
    marginTop: 15,
    backgroundColor: "white",
    width: "80%",
    height: "7%",
    borderRadius: 10,
  },
  comboTitle: {
    fontSize: 23,
    marginTop: 15,
  },
  productCard: {
    flex: 1,
    justifyContent: "center",
    padding: 15,
    marginTop: 15,
    alignItems: "center",
    marginHorizontal: 12,
    backgroundColor: "white",
    borderRadius: 20,
    width: 150,
    height: 150,
  },
  productImage: {
    width: 62,
    height: 62,
  },
  productTitle: {
    marginVertical: 10,
  },
  productPrice: {
    color: "#339273",
  },
  loveProduct: {
    alignSelf: "flex-end",
    width: 16,
    height: 16,
  },
  plusProduct: {
    marginLeft: 30,
    width: 24,
    height: 24,
  },
  listKategori: {
    flexDirection: "row",
    marginVertical: 20,
    justifyContent: "center",
  },
  productCardList: {
    flex: 1,
    justifyContent: "center",
    padding: 15,
    marginTop: 15,
    alignItems: "center",
    marginHorizontal: 12,
    backgroundColor: "white",
    borderRadius: 20,
    width: 110,
    height: 110,
  },
  productImageList: {
    width: 35,
    height: 35,
  },
  productTitleList: {
    marginVertical: 10,
    fontSize: 12,
  },
  productPriceList: {
    color: "#339273",
    fontSize: 12,
  },
  loveProductList: {
    alignSelf: "flex-end",
    width: 12,
    height: 12,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOpacity: 1,
  },
  plusProductList: {
    marginLeft: 10,
    width: 16,
    height: 16,
  },
  listKategori: {
    flexDirection: "row",
    marginVertical: 20,
    justifyContent: "center",
  },
});
