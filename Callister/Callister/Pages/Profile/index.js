import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View, Image } from "react-native";
import firebase from "firebase";

export default function ProfileScreen(navigation) {
  const [data, setData] = useState({});
  const [dataKey, setDataKey] = useState([]);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={{ fontSize: 20 }}>My Account</Text>
        <View style={styles.headerList}>
          <TouchableOpacity>
            <Image
              style={styles.notif}
              source={require("../../Asset/notif.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image
              style={styles.message}
              source={require("../../Asset/message.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image
              style={styles.setting}
              source={require("../../Asset/setting.png")}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View styles={styles.image}>
        <Image
          style={styles.imageDetail}
          source={require("../../Asset/profile.png")}
        />
      </View>
      <View style={{ alignSelf: "flex-start", marginLeft: "8%" }}>
        <Text
          style={{ fontSize: 35, fontWeight: "bold", position: "absolute" }}
        >
          Profile
        </Text>
      </View>
      <View style={styles.conten}>
        <View style={styles.subConten}>
          <Text style={{ fontSize: 16, fontWeight: "bold" }}>Full Name</Text>
          <Text style={{ fontSize: 16, color: "grey" }}>
            M. Dzikri Alfarisyi
          </Text>
          <View style={styles.line}></View>
          <Text style={{ fontSize: 16, fontWeight: "bold" }}>Email</Text>
          <Text style={{ fontSize: 15, color: "grey" }}>dzik@gmail.com</Text>
          <View style={styles.line}></View>
          <Text style={{ fontSize: 16, fontWeight: "bold" }}>Address</Text>
          <Text style={{ fontSize: 16, color: "grey" }}>
            Kp. Purwaraja Desa. Malingping, Kecamatan Menes Pandeglang - Banten
            Kode Pos 42262
          </Text>
          <TouchableOpacity style={styles.about}>
            <Text style={{ fontWeight: "bold" }}>About the app ></Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#B5E7DB",
    alignItems: "center",
  },
  header: {
    margin: 30,
    alignSelf: "flex-start",
    flexDirection: "row",
  },
  image: {
    padding: 50,
  },
  imageDetail: {
    width: 175,
    height: 175,
  },
  conten: {
    marginTop: "12%",
    width: "100%",
    height: "60%",
    borderTopStartRadius: 30,
    borderTopEndRadius: 30,
    backgroundColor: "#E7EFF4",
  },
  headerList: {
    flexDirection: "row",
    marginLeft: "40%",
  },
  notif: {
    width: 18,
    height: 24,
  },
  message: {
    width: 27,
    height: 27,
    marginHorizontal: 15,
  },
  setting: {
    width: 27,
    height: 27,
  },
  line: {
    width: "100%",
    height: 0.5,
    backgroundColor: "grey",
    marginVertical: 12,
  },
  subConten: {
    margin: "8%",
  },
  about: {
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    width: "40%",
    height: 35,
    borderRadius: 20,
    alignSelf: "flex-end",
    marginTop: 30,
  },
});
