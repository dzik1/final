import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
} from "react-native";

export default function AboutScreen() {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.subContainer}>
        <View style={styles.header}>
          <View style={styles.subHeader}>
            <Text style={styles.titleHeader}>About</Text>
            <Image
              style={styles.imageHeader}
              source={require("../../Asset/pen.png")}
            />
          </View>
          <View style={styles.founder}>
            <Image
              style={styles.imageFounder}
              source={require("../../Asset/Founder.png")}
            />
          </View>
        </View>

        <View style={styles.contact}>
          <Text style={styles.titleContact}>Contact</Text>
          <View style={{ flexDirection: "row", marginBottom: 10 }}>
            <TouchableOpacity>
              <Image
                style={{ marginRight: 5 }}
                source={require("../../Asset/WhatsApp.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={{ marginLeft: 5 }}
                source={require("../../Asset/Facebook.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity>
              <Image
                style={{ marginRight: 5 }}
                source={require("../../Asset/Twitter.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Image
                style={{ marginLeft: 5 }}
                source={require("../../Asset/Discord.png")}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.footer}>
          <Text style={styles.titleFooter}>Portfolio</Text>
          <TouchableOpacity>
            <Image
              style={{ marginBottom: 10 }}
              source={require("../../Asset/Gitlab.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image source={require("../../Asset/LinkedIn.png")} />
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    resizeMode: "cover",
    backgroundColor: "#E7EFF4",
    alignItems: "center",
  },
  subContainer: {
    margin: 10,
  },
  imageFounder: {
    marginBottom: 20,
  },
  subHeader: {
    flexDirection: "row",
    marginTop: 10,
    position: "absolute",
  },
  titleHeader: {
    fontSize: 30,
    fontWeight: "bold",
  },
  imageHeader: {
    marginLeft: 10,
  },
  founder: {
    marginTop: 20,
  },
  titleContact: {
    marginTop: 5,
    marginBottom: 10,
    fontSize: 25,
  },
  footer: {
    alignItems: "center",
    marginTop: 5,
  },
  titleFooter: {
    fontSize: 25,
    marginBottom: 10,
  },
});
