import React, { useState } from "react";
import {
  StyleSheet,
  ImageBackground,
  Image,
  Text,
  View,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Button,
  Alert,
} from "react-native";

import firebase from "firebase/app";

export default function RegisterScreen({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [fullname, setfullname] = useState("");
  const [address, setAddress] = useState("");
  const [isError, setIsError] = useState(false);

  const submit = () => {
    const Data = {
      email,
      password,
      fullname,
      address,
    };
    console.log(Data);

    if (email && password && fullname && address) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then((userCredential) => {
          // Signed in
          var user = userCredential.user;
          console.log(user);
          // ...
        })
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;
          // ..
        });

      const data = firebase.database().ref("Data");
      const listData = {
        fullname: fullname,
        email: email,
        address: address,
      };

      data
        .push(listData)
        .then((a) => {
          Alert.alert("Sukses", "Data Tersimpan");
          navigation.replace("LoginScreen");
        })
        .catch((error) => {
          console.log("Error : ", error);
        });
    } else {
      Alert.alert("Error", "Semua From Wajib Diisi");
    }
  };

  return (
    <ImageBackground
      source={require("../../Asset/bg3.png")}
      style={styles.image}
    >
      <ScrollView>
        <SafeAreaView style={styles.container}>
          <View style={styles.loginContainer}>
            <Text style={styles.titlelogin}>Register</Text>
            <View style={styles.fromContainer}>
              <View style={styles.form}>
                <TextInput
                  style={styles.fromInput}
                  placeholder="Full Name"
                  value={fullname}
                  onChangeText={(value) => setfullname(value)}
                />
                <TextInput
                  style={styles.fromInput}
                  placeholder="Email"
                  keyboardType="email-address"
                  value={email}
                  onChangeText={(value) => setEmail(value)}
                />
                <TextInput
                  style={styles.fromInput}
                  placeholder="Password"
                  keyboardType="visible-password"
                  value={password}
                  onChangeText={(value) => setPassword(value)}
                />
                <TextInput
                  style={styles.fromInputArea}
                  placeholder="Address"
                  multiline={true}
                  numberOfLines={4}
                  onChangeText={(value) => setAddress(value)}
                />
              </View>
            </View>
            <View style={styles.bottomContainer}>
              <View>
                <TouchableOpacity style={styles.buttonLogin} onPress={submit}>
                  <Text
                    style={{ color: "white", fontSize: 20, fontWeight: "bold" }}
                  >
                    Register
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </SafeAreaView>
        <SafeAreaView style={styles.footer}>
          <TouchableOpacity>
            <Image
              style={styles.iconGoogle}
              source={require("../../Asset/iconGoogle.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image
              style={{ marginLeft: 40 }}
              source={require("../../Asset/iconFb.png")}
            />
          </TouchableOpacity>
          <View style={styles.titleFooter}>
            <Text style={{ color: "white", fontSize: 16 }}>
              Already a Member?
            </Text>
            <TouchableOpacity
              onPress={() => navigation.navigate("LoginScreen")}
            >
              <Text
                style={{ color: "white", fontSize: 16, fontWeight: "bold" }}
              >
                Login
              </Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </ScrollView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    marginTop: 70,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
  },
  loginContainer: {
    margin: 20,
  },
  titlelogin: {
    color: "black",
    fontSize: 36,
    fontWeight: "bold",
    marginTop: 50,
    marginBottom: 30,
  },
  fromContainer: {
    marginBottom: 20,
  },
  fromInput: {
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 8,
    padding: 10,
    margin: 10,
  },
  bottomContainer: {
    paddingTop: 15,
  },
  buttonLogin: {
    padding: 10,
    paddingHorizontal: 40,
    marginLeft: 70,
    borderRadius: 8,
    backgroundColor: "#C1EFE0",
    alignItems: "center",
  },
  footer: {
    flex: 1,
    flexDirection: "row",
    paddingBottom: 20,
    margin: 20,
    marginTop: 50,
  },
  titleFooter: {
    alignItems: "flex-start",
    marginLeft: 60,
    paddingTop: 10,
    color: "white",
  },
  fromInputArea: {
    borderWidth: 1,
    borderColor: "white",
    borderRadius: 8,
    padding: 10,
    margin: 10,
    textAlignVertical: "top",
  },
});
