import Home from "./Home.png";
import HomeAktif from "./HomeAktif.png";
import Basket from "./Basket.png";
import BasketAktif from "./BasketAktif.png";
import Profile from "./Profile.png";
import ProfileAktif from "./ProfileAktif.png";

export { Home, HomeAktif, Basket, BasketAktif, Profile, ProfileAktif };
