import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";

import SplashSceen from "../Pages/SplashScreen";
import RegisterScreen from "../Pages/Register/index";
import LoginScreen from "../Pages/Login";
import AboutScreen from "../Pages/About";
import HomeScreen from "../Pages/Home";
import DetailScreen from "../Pages/DetailProduct";
import ProfileScreen from "../Pages/Profile";
import BasketScreen from "../Pages/Basket";
import { BottomNavigation } from "../Components";

const Tab = createBottomTabNavigator();
const Drawwer = createDrawerNavigator();
const Stact = createStackNavigator();

export default function Router() {
  return (
    <NavigationContainer>
      <Stact.Navigator initialRouteName="SplashSceen">
        <Stact.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{ headerShown: false }}
        />
        <Stact.Screen
          name="SplashSceen"
          component={SplashSceen}
          options={{ headerShown: false }}
        />
        <Stact.Screen
          name="RegisterScreen"
          component={RegisterScreen}
          options={{ headerShown: false }}
        />
        <Stact.Screen
          name="MainApp"
          component={MainApp}
          options={{ headerShown: false }}
        />
        <Stact.Screen
          name="DetailScreen"
          component={DetailScreen}
          options={{ headerShown: false }}
        />
        <Stact.Screen
          name="AboutScreen"
          component={AboutScreen}
          options={{ headerShown: false }}
        />
      </Stact.Navigator>
    </NavigationContainer>
  );
}

const MainApp = () => (
  <Tab.Navigator tabBar={(props) => <BottomNavigation {...props} />}>
    <Tab.Screen
      name="Home"
      component={HomeScreen}
      options={{ headerShown: false }}
    />
    <Tab.Screen
      name="About"
      component={AboutScreen}
      options={{ headerShown: false }}
    />
    <Tab.Screen
      name="Profile"
      component={ProfileScreen}
      options={{ headerShown: false }}
    />
  </Tab.Navigator>
);
