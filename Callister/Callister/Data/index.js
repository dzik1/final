const Recomended = [
  {
    id: "1",
    harga: "50.000",
    image: require("../Asset/taichan.png"),
    title: "Taichan Satay",
    desc: "A pack of taichan satay, a plate of rice, and a glass of sweet iced tea",
    love: require("../Asset/love.png"),
    plus: require("../Asset/plus.png"),
  },
  {
    id: "2",
    harga: "50.000",
    image: require("../Asset/taichan.png"),
    title: "Taichan Satay",
    desc: "A pack of taichan satay, a plate of rice, and a glass of sweet iced tea",
    love: require("../Asset/love.png"),
    plus: require("../Asset/plus.png"),
  },
];

export { Recomended };

const Product = [
  {
    id: "1",
    harga: "50.000",
    image: require("../Asset/taichan.png"),
    title: "Taichan Satay",
    desc: "A pack of taichan satay, a plate of rice, and a glass of sweet iced tea",
    love: require("../Asset/love.png"),
    plus: require("../Asset/plus.png"),
    bgColor: "#FFFAEB",
  },
  {
    id: "2",
    harga: "50.000",
    image: require("../Asset/taichan.png"),
    title: "Taichan Satay",
    desc: "A pack of taichan satay, a plate of rice, and a glass of sweet iced tea",
    love: require("../Asset/love.png"),
    plus: require("../Asset/plus.png"),
    bgColor: "#FEF0F0",
  },
  {
    id: "3",
    harga: "50.000",
    image: require("../Asset/taichan.png"),
    title: "Taichan Satay",
    desc: "A pack of taichan satay, a plate of rice, and a glass of sweet iced tea",
    love: require("../Asset/love.png"),
    plus: require("../Asset/plus.png"),
    bgColor: "#C9E5F6",
  },
  {
    id: "4",
    harga: "50.000",
    image: require("../Asset/taichan.png"),
    title: "Taichan Satay",
    desc: "A pack of taichan satay, a plate of rice, and a glass of sweet iced tea",
    love: require("../Asset/love.png"),
    plus: require("../Asset/plus.png"),
    bgColor: "#FFFAEB",
  },
  {
    id: "5",
    harga: "50.000",
    image: require("../Asset/taichan.png"),
    title: "Taichan Satay",
    desc: "A pack of taichan satay, a plate of rice, and a glass of sweet iced tea",
    love: require("../Asset/love.png"),
    plus: require("../Asset/plus.png"),
    bgColor: "#FEF0F0",
  },
  {
    id: "6",
    harga: "50.000",
    image: require("../Asset/taichan.png"),
    title: "Taichan Satay",
    desc: "A pack of taichan satay, a plate of rice, and a glass of sweet iced tea",
    love: require("../Asset/love.png"),
    plus: require("../Asset/plus.png"),
    bgColor: "#C9E5F6",
  },
];

export { Product };
