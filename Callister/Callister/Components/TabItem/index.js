import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  Home,
  HomeAktif,
  Basket,
  BasketAktif,
  Profile,
  ProfileAktif,
} from "../../Asset/index";

const TabItem = ({ isFocused, onPress, onLongPress, label }) => {
  const Icon = () => {
    if (label === "Home") return isFocused ? <HomeAktif /> : <Home />;
    if (label === "About") return isFocused ? <BasketAktif /> : <Basket />;
    if (label === "Home") return isFocused ? <ProfileAktif /> : <Profile />;

    return <HomeAktif />;
  };
  return (
    <TouchableOpacity
      onPress={onPress}
      onLongPress={onLongPress}
      style={styles.container}
    >
      {/* <Icon /> */}
      <Text style={styles.text(isFocused)}>{label}</Text>
    </TouchableOpacity>
  );
};

export default TabItem;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  text: (isFocused) => ({
    fontSize: 13,
    color: isFocused ? "black" : "grey",
  }),
});
